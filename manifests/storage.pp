class analysator::storage {
  package { 'centos-release-ceph-luminous':
    ensure => installed,
  }
  -> package { 'ceph-common':
    ensure => installed,
  }
  -> file { '/etc/ceph/ceph.conf':
    ensure => file,
    source => 'puppet:///modules/analysator/ceph.conf',
  }

  file { '/storage':
    ensure => directory,
  }
  -> file_line { 'ceph_storage_mount':
    path => '/etc/fstab',
    line => '10.44.1.100:6789:/users	/storage	ceph	name=analysator,mds_namespace=storage,secretfile=/etc/ceph/client.analysator.secret,noatime,_netdev	0	2',
  }
  file_line { 'ceph_home_mount':
    path => '/etc/fstab',
    line => '10.44.1.100:6789:/users /home ceph name=analysator,mds_namespace=home,secretfile=/etc/ceph/client.analysator.secret,noatime,_netdev 0 2',
  }
}
