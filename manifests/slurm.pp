class analysator::slurm {
  require ::analysator::munge
  package { [
      'mariadb-server',
      'mariadb-devel',
      'munge-libs',
      'munge-devel',
      'openssl',
      'openssl-devel',
      'pam-devel',
      'numactl',
      'numactl-devel',
      'hwloc-devel',
      'lua',
      'lua-devel',
      'readline-devel',
      'rrdtool-devel',
      'ncurses-devel',
      'man2html',
      'libibmad',
      'libibumad',
      'rpm-build',
      'gcc',
      'pmix',
      'pmix-devel',
    ]:
      ensure => installed,
  }

  $slurmversion = '18.08.4-1'
  $slurmurl = '18.08.4'

  # Compile slurm....
  file { '/root/.rpmmacros':
    ensure  => file,
    content => '%__make /usr/bin/make -j',
  }
  -> file { "/root/slurm-${slurmurl}.tar.bz2":
    ensure => file,
    source => "https://download.schedmd.com/slurm/slurm-${slurmurl}.tar.bz2",
  }
  ~> exec { 'build slurm':
    command     => "/usr/bin/rpmbuild -ta --with=pmix /root/slurm-${slurmurl}.tar.bz2",
    path        => '/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin',
    environment => 'HOME=/root',
    refreshonly => true,
  }
  ~> exec { 'install slurm':
    command     => 'yum -y --nogpgcheck localinstall /root/rpmbuild/RPMS/x86_64/slurm-*',
    path        => '/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin',
    cwd         => '/root/rpmbuild/RPMS/x86_64/',
    timeout     => '0',
    environment => 'HOME=/root',
    refreshonly => true,
  }

  user { 'slurm':
    uid    => '51',
    system => true,
  }
  -> file { '/opt/slurm/':
    ensure => directory,
    owner  => 'slurm',
  }
  -> file { '/opt/slurm/spool/':
    ensure => directory,
    owner  => 'slurm',
  }

  file { '/etc/slurm':
    ensure => directory,
  }
  -> file { '/etc/slurm/gres.conf':
    ensure => file,
    source => 'puppet:///modules/analysator/gres.conf',
  }
  -> file { '/etc/slurm/slurm.conf':
    ensure => file,
    source => 'puppet:///modules/analysator/slurm.conf',
  }
  -> file { '/etc/slurm/slurm_jobcomp_logger':
    ensure => file,
    source => 'puppet:///modules/analysator/slurm_jobcomp_logger',
  }

  file { '/var/log/slurm/':
    ensure => directory,
    owner  => 'slurm',
    group  => 'slurm',
    mode   => '0755',
  }

  # FIXME: accounting should not be needed on clients?
  file { '/var/log/slurm/accounting/':
    ensure => directory,
    owner  => 'slurm',
    group  => 'slurm',
    mode   => '0755',
  }

  file { ['/var/log/slurm_jobacct.log',]:
    ensure => file,
    owner  => 'slurm',
  }

}
