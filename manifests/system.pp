class analysator::system::interfaces {
  # Four networks
  # - Public IP             130.236.254.$last_octet    enp7s0f0
  # - Mgmt                  10.42.0.node-id/24      
  # - Internal Ethernet     10.41.0.node-id/24         enp7s0f1
  # - IPoIB                 10.44.4.node-id/16         ib1

  ipoib::interface { $analysator::system::ib_iface:
    ipaddress => '10.44.4.1',
    netmask   => '255.255.0.0',
  }

  -> network::interface { $analysator::system::internal_iface:
    ipaddress            => '10.41.0.1',
    netmask              => '255.255.255.0',

    options_extra_redhat => {
      'DEFROUTE' => 'no',
      'ONBOOT'   => 'yes',
    },
  }
}

class analysator::system::nat {
  require ::lysnetwork::iptables
  sysctl { 'net.ipv4.ip_forward':
    ensure => present,
    value  => '1',
  }

  firewall { '200 forward internal':
    chain    => 'FORWARD',
    iniface  => $analysator::system::internal_iface,
    outiface => $analysator::system::public_iface,
    proto    => 'all',
    action   => 'accept',
  }

  firewall { '201 forward internal':
    chain    => 'FORWARD',
    outiface => $analysator::system::internal_iface,
    iniface  => $analysator::system::public_iface,
    proto    => 'all',
    action   => 'accept',
  }

  firewall { '202 forward ib':
    chain    => 'FORWARD',
    outiface => $analysator::system::public_iface,
    iniface  => $analysator::system::ib_iface,
    proto    => 'all',
    action   => 'accept',
  }

  firewall { '203 forward ib':
    chain    => 'FORWARD',
    outiface => $analysator::system::ib_iface,
    iniface  => $analysator::system::public_iface,
    proto    => 'all',
    action   => 'accept',
  }


  firewall { '204 nat for internal':
    chain    => 'POSTROUTING',
    jump     => 'MASQUERADE',
    proto    => 'all',
    outiface => $analysator::system::public_iface,
    table    => 'nat',
  }
}

class analysator::system::dhcp {
  require ::analysator::system::interfaces
  package { 'dhcp':
    ensure => installed,
  }

  file { '/etc/dhcp/dhcpd.conf':
    ensure  => file,
    source  => 'puppet:///modules/analysator/system/dhcpd.conf',
    require => Package['dhcp'],
  }

  service { 'dhcpd':
    ensure    => running,
    enable    => true,
    subscribe => [ Package['dhcp'], File['/etc/dhcp/dhcpd.conf'] ],
    require   => [ Package['dhcp'], File['/etc/dhcp/dhcpd.conf'] ],
  }
}

class analysator::system::pxe_install {
  package { ['tftp-server','xinetd','syslinux','vsftpd']:
    ensure => installed,
  }
  -> file { '/var/lib/tftpboot/':
    ensure  => directory,
    source  => 'file:/usr/share/syslinux/',
    recurse => true,
  }
  -> file { '/var/lib/tftpboot/pxelinux.cfg/':
    ensure => directory,
  }
  -> file { '/var/lib/tftpboot/pxelinux.cfg/default':
    ensure => file,
    source => 'puppet:///modules/analysator/system/pxelinux.cfg',
  }
  -> file { '/var/lib/tftpboot/centos/':
    ensure => directory,
  }
  -> file { '/var/lib/tftpboot/initrd.img':
    ensure => file,
    source => 'http://ftp.lysator.liu.se/centos/7/os/x86_64/isolinux/initrd.img',
  }
  -> file { '/var/lib/tftpboot/vmlinuz':
    ensure => file,
    source => 'http://ftp.lysator.liu.se/centos/7/os/x86_64/isolinux/vmlinuz',
  }
  -> service { 'tftp':
    ensure => stopped,
    enable => false,
  }
  -> file { '/etc/xinetd.d/tftp':
    ensure => file,
    source => 'puppet:///modules/analysator/system/xinetd.d-tftp',
  }

  service { 'xinetd':
    ensure    => running,
    enable    => true,
    require   => [ Package['xinetd'], File['/etc/xinetd.d/tftp']],
    subscribe => [ Package['xinetd'], File['/etc/xinetd.d/tftp']],
  }

  service { 'vsftpd':
    ensure    => stopped,
    enable    => false,
    require   => Package['vsftpd'],
    subscribe => Package['vsftpd'],
  }

  file { '/usr/share/nginx/html/ks.cfg':
    ensure => file,
    source => 'puppet:///modules/analysator/system/node-ks.cfg',
    mode   => '0444',
  }

  include ::nginx
  nginx::resource::server { '10.41.0.1':
    listen_port => 80,
    www_root    => '/usr/share/nginx/html/',
  }

}

class analysator::system {
  $public_iface = 'enp7s0f0'
  $internal_iface = 'enp7s0f1'
  $ib_iface = 'ib1'
  require ::analysator::system::interfaces
  include ::analysator::system::nat
  include ::analysator::system::dhcp
  include ::analysator::system::pxe_install
  include ::analysator::packages::system
  include ::analysator::hosts
  require ::analysator::slurm

  service { 'slurmctld':
    ensure    => running,
    enable    => true,
    subscribe => File['/etc/slurm/slurm.conf'],
  }
}
