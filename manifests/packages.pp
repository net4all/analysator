# Should be installed everywhere
class analysator::packages::basic
{
  package {
    [
      'emacs',             # We are not animals
      'ctags-etags',
      'ctags',
    ]:
      ensure => installed;
  }
}

# Packages needed for running compute workloads
class analysator::packages::compute_node
{
  require analysator::packages::basic
  package {
    [
      'flatpak-builder',   # FIXME: move to build_node?
      'gcc-c++',
      'opencl-headers',
      'opencl-filesystem',
      'ocl-icd',
      'perf',
      'ruby',
      'guile',
    ]:
      ensure => installed,
  }

  package {
    [
      'autofs',  # auto.home will override the ceph /home mount
    ]:
      ensure => purged,
  }
}

# Packages needed for building, but not needed when running
# (In practise we only have build_node's at the moment)
class analysator::packages::build_node
{
  require analysator::packages::compute_node
  package {
    [
      'autoconf',
      'automake',
      'bison',
      'bzip2-devel',
      'cmake',
      'clang',
      'flex',
      'ImageMagick-devel',
      'libtool',
      'libjpeg-turbo-devel',
      'libtiff-devel',
      'python3',
      'ruby-devel',
      'ucx-static', # unknown for what, but it's installed on almost all nodes
      'qt5-qtbase-devel',  # Mame requirement /zino
      'qt-devel',          # Old Mame requirement, useful but will not cry if it's removed /zino
      'SDL2-devel',        # Mame requirement /zino
      'SDL2_ttf-devel',    # Mame requirement /zino
      'libXi-devel',       # Mame requirement /zino
      'alsa-lib-devel',    # Mame requirement /zino
      'fontconfig-devel',  # Mame requirement /zino
      'libXinerama-devel', # Mame requirement /zino
      'libsqlite3x-devel', # For SQlite module in Pike /zino
    ]:
      ensure => installed;
  }
}

# Packages needed for running compute on Nvidia GPUs
#
# NOTE: The early installations on n[1593,1599] by hx where manually
# installed via
#     http://us.download.nvidia.com/tesla/410.79/nvidia-diag-driver-local-repo-rhel7-410.79-1.0-1.x86_64.rpm
# No attempt has been made to perfectly clean them up
class analysator::packages::gpu_node
{
  require analysator::repos::cuda
  package {
    [
      'cuda',
    ]:
      ensure => installed,
  }
}

# Packages that makes life easier on the system node
class analysator::packages::system
{
  require analysator::packages::basic
  package {
    [
      'alien',	# Handy package conversion tool
    ]:
      ensure => installed,
  }
}
