class analysator::node::network
(
  $login = false,
  $public_ip = undef,
) {
  require ::lysnetwork::iptables
  $last_octet = split($facts['networking']['interfaces']['eno1']['bindings'][0]['address'], '\.')[3]
  $ib_iface = 'ib0'
  $eth_iface = 'eno1'

  if(!$login) {
    ipoib::interface { $ib_iface:
      ipaddress => "10.44.4.${last_octet}",
      netmask   => '255.255.0.0',
      gateway   => '10.44.4.1',
      domain    => 'lysator.liu.se',
      dns1      => '130.236.254.4',
      dns2      => '130.236.254.225',
    }
  }
}

class analysator::node (
  $login = false,
){
  include ::stdlib
  include ::analysator::common
  require ::analysator::slurm
  include ::analysator::munge

  if(!$login) {
    service { 'slurmd':
      ensure    => running,
      enable    => true,
      subscribe => File['/etc/slurm/slurm.conf'],
    }
  }

  ssh_authorized_key { 'root@analysator-system':
    ensure => present,
    user   => 'root',
    type   => 'ssh-rsa',
    key    => 'AAAAB3NzaC1yc2EAAAADAQABAAABAQDEjpMM8PThEq6ZWksTHjH+zklicY2m/la7VS1FTXOUz2G0Z6eRI22icgSgVNKowuPsfEp0f1YKwmcdiMOOZLp4zvqdbIJEWwZRcXqbHhpQMgub+Wfu8i91CY/CCnniQen1LQzqDed3X/wOTjkfEiNMAUZo1pQnuaOYY/RmwPlmLsZVaTgNsqF8g6deVBQ463T4x3S5MajUgfEwIeLhv7k2aCFayrqRMhPl1ek5Tyw3FEngXi15ah+Jyt/pNK5vE/cH+jskEpd8vw6zYSJUCKvdnqfb6jzKQYRuqXXMYazntvBuPfl4iES+uc9XNEXMVFNO1wu547lE70lCspwlmuEl',
  }

  file_line { 'soft memlimit':
    path => '/etc/security/limits.conf',
    line => '* soft memlock unlimited',
  }

  file_line { 'hard memlimit':
    path => '/etc/security/limits.conf',
    line => '* hard memlock unlimited',
  }

  package { 'environment-modules':
    ensure => absent,
  }

  package { ['Lmod', 'python2-pip']:
    ensure => installed,
  }

  file { '/sw':
    ensure => directory,
  }
  -> file_line { 'mount analysator-sw':
    path => '/etc/fstab',
    line => 'home:/ceph-home/analysator-sw    /sw    nfs    defaults,bg    0 0',
  }
  ~> exec { '/usr/bin/mount /sw':
    refreshonly => true,
  }
  file { '/etc/profile.d/01-analysator-modulepath.sh':
    ensure  => file,
    content => 'export MODULEPATH=/sw/easybuild/modules/all:$MODULEPATH',
  }

  package { ['dkms', 'kernel-devel', 'elfutils' ]:
    ensure => installed,
  }

  include analysator::packages::compute_node
  include ::analysator::storage
}
