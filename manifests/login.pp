class analysator::login {
  include analysator::packages::build_node

  $eth_iface='eno2'
  network::interface { $eth_iface:
    ipaddress => '130.236.254.181',
    netmask   => '255.255.255.0',
  }
}
