class analysator::prometheus {
  class {'::prometheus::server':
    version        => '2.0.0',
    scrape_configs => [
      { 'job_name'        => 'prometheus_analysator',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          { 'targets' => [ 'localhost:9090' ],
        'labels'      => { 'alias' => 'Prometheus'},}]
      },
      { 'job_name'        => 'ceph-mgr',
        'scrape_interval' => '30s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          { 'targets' => [ 'vogon-0:9283' ],
            'labels'  => { 'alias' => 'Ceph'}
          }
        ]
      },
      { 'job_name'        => 'node_exporter_ceph',
        'scrape_interval' => '30s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          { 'targets' => [
              'vogon-0:9100',
              'vogon-1:9100',
              'vogon-2:9100',
              'trillian-0:9100',
              'trillian-1:9100',
              'trillian-2:9100',
              'trillian-3:9100',
              'trillian-4:9100',
              'trillian-5:9100',
              'trillian-6:9100',
              'trillian-7:9100',
              'babelfish:9100',
            ],
          }
        ]
      },
      { 'job_name'        => 'node_exporter_services',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          { 'targets' => [
              'graf:9100',
              'proxdisk:9100',
            ],
          }
        ]
      },

      { 'job_name'        => 'node_exporter_analysator',
        'scrape_interval' => '10s',
        'scrape_timeout'  => '10s',
        'static_configs'  => [
          { 'targets' => [
              'n1600:9100',
              'n1585:9100',
              'n1586:9100',
              'n1587:9100',
              'n1588:9100',
              'n1593:9100',
              'n1594:9100',
              'n1595:9100',
              'n1596:9100',
              'n1599:9100',
            ],
          }
        ]
      },


    ],
  }


}
